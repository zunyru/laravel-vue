<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Blog extends Model
{
    use LogsActivity;

    protected $table = 'blog';

    protected $fillable = ['title','content'];

    protected static $logName = 'custom_Blog';

    public function getDescriptionForEvent(string $eventName): string
    {
        return "This model has been {$eventName}";
    }
}
